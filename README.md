# Pd particles



## Contents
Both surfaces Pd100 and Pd211 are in individual folders


Each structure is named x_y_y.cif

- x is the initial state of a reaction
- y is the final state of a reaction
- z descriptor of specific structure, is, fs, or ts

is - initial state (x) 

fs - final state (y) 

ts - transition state between x and y

